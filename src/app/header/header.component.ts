import { Component } from '@angular/core';

@Component({
  selector: 'app-header',
  standalone: true,
  imports: [],
  templateUrl: './header.component.html',
  styleUrl: './header.component.css'
})

export class HeaderComponent {
  
  login() {
    alert('Welcome!');
  }
  
  title = "Mon application de notes";
  isHidden = "true";
  imageURL = "https://bocdn.ecotree.green/blog/0001/02/81cdd8386afabed5c1b09f24d94dba05758a08d5.jpg?d=1920x1080";
}
