import { Component } from '@angular/core';
import {RouterModule } from '@angular/router';
import {HeaderComponent} from './header/header.component';
import {NotesListComponent} from './notes-list/notes-list.component';
import { AddNoteComponent } from './add-note/add-note.component';

@Component({
  selector: 'app-root',
  standalone: true,
  imports: [RouterModule,AddNoteComponent,HeaderComponent,NotesListComponent],
  templateUrl: './app.component.html',
  styleUrl: './app.component.css'
})
export class AppComponent {
  title = 'Notes';
}
