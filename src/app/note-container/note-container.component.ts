import { Component,inject } from '@angular/core';
import { NOTES } from '../utils/notes';
import { RouterModule,ActivatedRoute,Router } from '@angular/router';

@Component({
  selector: 'app-note-container',
  standalone: true,
  imports: [RouterModule],
  templateUrl: './note-container.component.html',
  styleUrl: './note-container.component.css'
})
export class NoteContainerComponent {
  activeRoute = inject(ActivatedRoute);
  router = inject(Router);
  id = Number(this.activeRoute.snapshot.paramMap.get('id'));
  note = NOTES.find((i) => i.id === this.id);

  delete(){
    if(this.note != undefined){
      let index = NOTES.indexOf(this.note, 0);
      NOTES.splice(index, 1);
    }
    this.router.navigateByUrl('/');
  }
}